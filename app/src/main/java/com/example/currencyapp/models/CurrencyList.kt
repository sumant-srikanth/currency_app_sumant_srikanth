package com.example.currencyapp.models

data class CurrencyList(val base: CurrencyData?, val currencyList: List<CurrencyData>)