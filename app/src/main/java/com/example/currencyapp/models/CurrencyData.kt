package com.example.currencyapp.models

data class CurrencyData(val name: String, var rate: Double, var result: String)