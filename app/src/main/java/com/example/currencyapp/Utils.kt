package com.example.currencyapp

import java.text.DecimalFormat

class Utils {
    companion object {
        fun convertTo2SF(s: Double): String {
            val df2 = DecimalFormat("#.##")
            return df2.format(s).toString()
        }

    }
}