package com.example.currencyapp.repos

import com.example.currencyapp.models.CurrencyResponse
import io.reactivex.Observable

interface CurrencyRepo {
    fun fetchCurrencies(): Observable<CurrencyResponse>
}