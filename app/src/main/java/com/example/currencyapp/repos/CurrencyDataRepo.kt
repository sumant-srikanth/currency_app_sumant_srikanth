package com.example.currencyapp.repos

import com.example.currencyapp.api.CurrencyService
import com.example.currencyapp.models.CurrencyResponse
import io.reactivex.Observable
import javax.inject.Inject

class CurrencyDataRepo @Inject constructor(private val currencyService: CurrencyService) :
    CurrencyRepo {

    override fun fetchCurrencies(): Observable<CurrencyResponse> {
        return currencyService.getCurrencies()
    }

}