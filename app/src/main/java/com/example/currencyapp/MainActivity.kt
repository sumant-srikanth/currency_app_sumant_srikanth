package com.example.currencyapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.currencyapp.adapters.CurrencyListAdapter
import com.example.currencyapp.models.CurrencyData
import com.example.currencyapp.models.CurrencyResponse
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as MyApp).appComponent.inject(this)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val adapter = CurrencyListAdapter(object : CurrencyListAdapter.CurrencySelectionListener {
            override fun onCurrencySelected(cur: CurrencyData) {
                mainViewModel.selectedCurrency.value = cur
            }
        })

        rv_currencies.layoutManager = LinearLayoutManager(this)
        rv_currencies.itemAnimator = DefaultItemAnimator()
        rv_currencies.adapter = adapter

        mainViewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        mainViewModel.data.observe(this, Observer<List<CurrencyData>> { currencyList ->
            adapter.setData(currencyList)
        })

        mainViewModel.selectedCurrency.observe(this, Observer<CurrencyData> { cur ->
            adapter.updateSelectedItem(cur)
        })
    }
}
