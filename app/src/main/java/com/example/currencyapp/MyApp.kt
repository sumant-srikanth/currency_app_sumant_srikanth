package com.example.currencyapp

import android.app.Application
import com.example.currencyapp.di.AppComponent
import com.example.currencyapp.di.DaggerAppComponent
import com.example.currencyapp.di.modules.NetworkModule

class MyApp : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().networkModule(NetworkModule(AppConstants.BASE_URL)).build()
    }

    override fun onCreate() {
        super.onCreate()
    }
}