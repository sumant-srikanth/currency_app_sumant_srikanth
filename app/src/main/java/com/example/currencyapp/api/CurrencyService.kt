package com.example.currencyapp.api

import com.example.currencyapp.models.CurrencyResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface CurrencyService {
    @GET("latest?base=EUR")
    fun getCurrencies(): Observable<CurrencyResponse>
}