package com.example.currencyapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyapp.R
import com.example.currencyapp.Utils
import com.example.currencyapp.models.CurrencyData
import java.util.*
import kotlin.collections.ArrayList


class CurrencyListAdapter(private val listener: CurrencySelectionListener) :
    RecyclerView.Adapter<CurrencyListAdapter.ViewHolder>() {

    private val dataList: ArrayList<CurrencyData> = ArrayList()
    private lateinit var context: Context

    fun setData(list: List<CurrencyData>) {
        if (dataList.size == 0) {
            dataList.addAll(list)
            notifyDataSetChanged()
        } else {
            list.forEach {
                val item = dataList.find { oldItem -> it.name == oldItem.name }
                if (item != null)
                    item.rate = it.rate
                else
                    dataList.add(it)
            }
            updateResults(dataList.getOrNull(0)?.result?.toDouble())
            notifyItemRangeChanged(1, dataList.size - 1)
        }
    }

    fun updateSelectedItem(newCur: CurrencyData) {
        for (i in 0 until dataList.size) {
            val cur = dataList[i]
            if (cur.name == newCur.name) {
                cur.result = newCur.result
                updateSelectedItem(i)
                return
            }
        }
    }

    private fun updateSelectedItem(pos: Int) {
        if (pos == 0)
            return

        val curItem = dataList.getOrNull(0)
        val newItem = dataList.getOrNull(pos)
        if (newItem != null && curItem != newItem) {
            Collections.swap(dataList, 0, pos)
            notifyItemMoved(pos, 0)
            notifyItemMoved(0, pos)
            notifyItemChanged(pos)
            notifyItemChanged(0)

            listener.onCurrencySelected(newItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (!::context.isInitialized)
            context = parent.context
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.adapter_currency,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataList[position]
        holder.name.setText(item.name)
        holder.result.tag = position
        holder.result.setText(item.result)

        if (position == 0) {
            holder.result.isEnabled = true
            holder.parent.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    android.R.color.holo_blue_dark
                )
            )
        } else {
            holder.result.isEnabled = false
            holder.parent.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    android.R.color.white
                )
            )
        }
    }

    private fun updateResults(newResult: Double?) {
        for (i in 1 until dataList.size) {
            val it = dataList[i]
            it.result =
                Utils.convertTo2SF(it.rate / (dataList[0].rate) * (newResult ?: 0.0))
        }
    }

    interface CurrencySelectionListener {
        fun onCurrencySelected(cur: CurrencyData)
    }

    inner class ViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        val parent: View = view.findViewById(R.id.parent)
        val name: TextView = view.findViewById(R.id.name)
        val result: EditText = view.findViewById(R.id.result)

        init {
            parent.setOnClickListener {
                updateSelectedItem(adapterPosition)
            }

            //result.filters = arrayOf<InputFilter>(InputFilterMinMax(0.0, 999999999.0))
            result.setOnTouchListener { view, motionEvent ->
                if (motionEvent.action == MotionEvent.ACTION_UP) {
                    updateSelectedItem(adapterPosition)
                }
                return@setOnTouchListener false
            }
            result.doOnTextChanged { ch, _, _, _ ->
                if (result.tag is Int) {
                    val pos = result.tag as Int
                    // Update all other values
                    if (pos == 0) {
                        var newResult: Double = 0.0
                        try {
                            newResult = ch.toString().toDouble()
                        } catch (ex: NumberFormatException) {
                            ex.printStackTrace()
                        }
                        dataList[pos].result = newResult.toString()
                        updateResults(newResult)
                        //notifyItemRangeChanged(1, dataList.size - 1)
                    }
                }
            }
        }
    }
}