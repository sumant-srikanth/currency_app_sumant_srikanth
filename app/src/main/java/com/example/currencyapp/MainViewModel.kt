package com.example.currencyapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.currencyapp.models.CurrencyData
import com.example.currencyapp.models.CurrencyList
import com.example.currencyapp.repos.CurrencyRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repo: CurrencyRepo) : ViewModel() {

    private val _data = MutableLiveData<List<CurrencyData>>()
    val data: LiveData<List<CurrencyData>>
        get() = _data

    val selectedCurrency: MutableLiveData<CurrencyData> =
        MutableLiveData<CurrencyData>().apply { value = dummyCur }

    companion object {
        private val dummyCur = CurrencyData("", 0.0, "")
    }

    private val disposable: Disposable

    init {
        disposable = repo.fetchCurrencies().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .repeatWhen { it.delay(1, TimeUnit.SECONDS) }
            .map { res ->
                val baseKey = res.base
                val currencyList = ArrayList<CurrencyData>()
                val baseCur: CurrencyData?

                // Add default (EUR) value manually if it does not exist
                if (!res.rates.any { cur -> cur.key == baseKey }) {
                    baseCur = CurrencyData(baseKey, 1.0, "100.00")
                    currencyList.add(baseCur)
                } else
                    baseCur = null

                res.rates.forEach {
                    val myVal = Utils.convertTo2SF(100.0 * it.value)
                    currencyList.add(
                        CurrencyData(
                            it.key,
                            it.value,
                            myVal
                        )
                    )
                }
                return@map CurrencyList(baseCur, currencyList)
            }
            .subscribe({
                _data.value = it.currencyList
                if (selectedCurrency.value == dummyCur)
                    selectedCurrency.value = it.base
            }, {
                it.printStackTrace()
            })
    }

    override fun onCleared() {
        disposable.dispose()
    }
}