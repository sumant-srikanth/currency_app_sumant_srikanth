package com.example.currencyapp.di

import com.example.currencyapp.MainActivity
import com.example.currencyapp.di.modules.CurrencyModule
import com.example.currencyapp.di.modules.NetworkModule
import com.example.currencyapp.di.modules.ViewModelModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, CurrencyModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}