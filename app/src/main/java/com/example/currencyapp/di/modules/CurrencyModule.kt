package com.example.currencyapp.di.modules

import com.example.currencyapp.api.CurrencyService
import com.example.currencyapp.repos.CurrencyDataRepo
import com.example.currencyapp.repos.CurrencyRepo
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class CurrencyModule {

    @Binds
    abstract fun bindCurrencyRepo(currencyRepo: CurrencyDataRepo): CurrencyRepo

    @Module
    companion object {
        @Singleton
        @JvmStatic
        @Provides
        fun getCurrencyService(retrofit: Retrofit): CurrencyService {
            return retrofit.create(CurrencyService::class.java)
        }
    }
}